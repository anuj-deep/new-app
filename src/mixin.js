const mixin = {
	data() {
		return {
			message: "hello",
			foo: "abc",
		};
	},
	created() {
		console.log("hello from the mixins");
	},
};

module.exports = mixin;
